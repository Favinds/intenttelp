package com.pens.favian.intenttelp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

public class MainActivity extends AppCompatActivity {
    TextView Label1;
    EditText text1;
    Button btnCallActivity2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            setContentView(R.layout.activity_main);
            Label1 = (TextView) findViewById(R.id.Label1);
            text1 = (EditText) findViewById(R.id.text1);
            btnCallActivity2 = (Button) findViewById(R.id.btnCallActivity2);
            btnCallActivity2.setOnClickListener(new ClickHandler());
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private class ClickHandler implements OnClickListener {

        @Override
        public void onClick(View view) {
            try {
                String myData = text1.getText().toString();
                Intent myActivity2 = new
                        Intent(Intent.ACTION_DIAL, Uri.parse(myData));
                startActivity(myActivity2);
            }
            catch (Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}